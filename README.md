These Lua scripts run over the CSV version of the mnist digits database
https://pjreddie.com/projects/mnist-in-csv/

### Running
`lua test.lua`

You can also run `lua -i test.lua` to access the datasets and the classifiers
after the script finishes running.

