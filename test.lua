-- Variables are global to allow lua -i test.lua

local n1, n2 = tonumber(arg[1]), tonumber(arg[2])

trab = require("trab")

print("Reading training set...")
training_set = trab.read_mnist_csv("mnist_train")

print("Training classifier...")
classifier = training_set:train_classifier_for{n1, n2, limit = 40}

print("Reading test set...")
testing_set = trab.read_mnist_csv("mnist_test")

print("Testing classifier...")
errors, total = testing_set:run_classifier(classifier)

print("Errors:", (errors / total) * 100 .. "%")

