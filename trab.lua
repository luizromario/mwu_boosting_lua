local M = {}

function M.read_mnist_csv(filename)
    local file = io.open(filename .. ".csv")
    local data = {}

    for l in file:lines() do
        local matches = string.gmatch(l, "%d+")
        local item = {
           label = tonumber(matches()),
           pixels = {}}

        for pix in matches do
            table.insert(item.pixels, tonumber(pix))
        end

        table.insert(data, item)
    end

    function data:filter_numbers(n1, n2)
        local filtered = {}

        for _, it in ipairs(self) do
            if it.label == n1 or it.label == n2 then
                table.insert(filtered, it)
            end
        end

        return filtered
    end


    -- args: [1] = n1, [2] = n2, limit
    function data:train_classifier_for(args)
        local filtered = self:filter_numbers(args[1], args[2])
        local cl = M.init_adaboost{
            data = filtered,
            n1 = args[1], n2 = args[2],
            limit = args.limit
        }
        
        return cl
    end

    function data:run_classifier(cl)
        local filtered = self:filter_numbers(cl.n1, cl.n2)
        local errors = 0

        for i, item in ipairs(filtered) do
            local predict = cl:classify(item.pixels)
            if predict ~= item.label then errors = errors + 1 end
        end

        return errors, #filtered
    end

    return data
end

-- Based on Section 4.1 of Freund, Schapire '96
-- args: data, n1, n2, limit
function M.init_adaboost(args)
    local classif = {
        n1 = args.n1, n2 = args.n2,
        hs = {},
        pix_idxs = {},
        betas = {}
    }

    local pix_cls = {}
    for i = 1, 28 * 28 do
        table.insert(pix_cls, M.init_pix_cl(args.n1, args.n2))
    end

    local weights = {}
    for i = 1, #args.data do
        table.insert(weights, 1)
    end
    
    -- Build pixel classifiers
    for i, item in ipairs(args.data) do
        for i, pix in ipairs(item.pixels) do
            pix_cls[i]:feed(pix, item.label)
        end
    end
    
    for _, w in ipairs(pix_cls) do
        w:build()
    end

    for _ = 1, args.limit do
        -- Normalized weights vector
        local weights_sum = 0
        for _, w in ipairs(weights) do
            weights_sum = weights_sum + w
        end
        
        local normalized = {}
        for _, w in ipairs(weights) do
            table.insert(normalized, w / weights_sum)
        end
        
        -- Select hypothesis based on weights
        local min_error = 2
        local min_error_idx = 0
        for i, cl in ipairs(pix_cls) do
            local e = 0
            local eabs = 0
            
            for j, item in ipairs(args.data) do
                local predict = cl:classify(item.pixels[i])
                
                if predict ~= item.label then
                    e = e + normalized[j]
                    eabs = eabs + 1
                end
            end
            
            if e < min_error then
                min_error = e
                min_error_idx = i
            end
        end
        
        -- Calculate error of hypothesis
        local h = pix_cls[min_error_idx]
        local epsilon = 0.01
        for i, item in ipairs(args.data) do
            local predict = h:classify(item.pixels[min_error_idx])
            
            if predict ~= item.label then
                epsilon = epsilon + normalized[i]
            end
        end
        local beta = epsilon / (1 - epsilon)
        
        table.insert(classif.hs, h)
        table.insert(classif.pix_idxs, min_error_idx)
        table.insert(classif.betas, beta)
        
        -- Adjust weights vector
        for i, item in ipairs(args.data) do
            local predict = h:classify(item.pixels[min_error_idx])
            
            if predict ~= item.label then
                weights[i] = weights[i] / beta
            end
        end
    end

    local betas_sum = 0
    for _, b in ipairs(classif.betas) do
        betas_sum = betas_sum + b
    end
    classif.log_inv_betas_sum = betas_sum
    
    -- Final hypothesis
    function classif:classify(img)
        local pred_sum = 0
        
        for i, h in ipairs(self.hs) do
            local _, predict = h:classify(img[self.pix_idxs[i]])
            
            pred_sum = pred_sum + predict * math.log(1 / self.betas[i])
        end
        
        return pred_sum >= self.log_inv_betas_sum / 2 and self.n2 or self.n1
    end

    return classif
end

function M.init_pix_cl(n1, n2)
    local cl = {
        plus_t = {sum = 0, count = 0},
        minus_t = {sum = 0, count = 0},
        n1 = n1, n2 = n2
    }

    function cl:feed(val, label)
        local function incr(cnt)
            cnt.sum = cnt.sum + val
            cnt.count = cnt.count + 1
        end

        if label == self.n2 then
            incr(self.plus_t)
        elseif label == self.n1 then
            incr(self.minus_t)
        end
    end

    function cl:build()
        local plusavg = self.plus_t.sum / self.plus_t.count
        local minusavg = self.minus_t.sum / self.minus_t.count

        self.up = plusavg > minusavg
        self.threshold = (plusavg + minusavg) / 2
        self.plus_t, self.minus_t = nil, nil
    end

    function cl:classify(val)
        local lt_threshold = val > self.threshold

        if self.up and lt_threshold then
            return self.n2, 1
        elseif self.up and not lt_threshold then
            return self.n1, 0
        elseif not self.up and lt_threshold then
            return self.n1, 0
        else
            return self.n2, 1
        end
    end

    return cl
end

function M.print_img(img)
    local str = ""
    local colors = {" ", ".", ",", '"', "o", "*", "@", "#"}

    for i, pix in ipairs(img) do
        if i % 28 == 1 then
            str = str .. "\n"
        end

        str = str .. colors[math.ceil((pix / 256) * #colors)]
    end

    return str
end

return M

